//
//  ViewController.swift
//  Marvel app
//
//  Created by kevin on 28/11/17.
//  Copyright © 2017 kevin. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {

    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var DescriptionTextView: UITextView!
    
    @IBOutlet weak var WeightLabel: UILabel!
    @IBOutlet weak var HeightLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    @IBAction func ConsultarButtonPressed(_ sender: Any) {
        
        /*Alamofire.request("https://pokeapi.co/api/v2/pokemon/1").responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
            }
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)") // original server data as UTF8 string
            }
        }*/
        
        Alamofire.request("https://pokeapi.co/api/v2/pokemon/4").responseObject { (response: DataResponse<Pokemon>) in
    
            let pokemon = response.result.value
            
            DispatchQueue.main.async {
                self.NameLabel.text = pokemon?.name ?? ""
                self.HeightLabel.text = "\(pokemon?.height ?? 0)"
                self.WeightLabel.text = "\(pokemon?.weight ?? 0)"
                
            }
        }
    }
}
