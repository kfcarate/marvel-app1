//
//  DetailViewController.swift
//  Marvel app
//
//  Created by kevin on 3/1/18.
//  Copyright © 2018 kevin. All rights reserved.
//

import UIKit
import Alamofire

class DetailViewController: UIViewController {

    var pokemon:Pokemon?
    
    @IBOutlet weak var pokemonImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(pokemon?.name)
        self.title = pokemon?.name
        
        let pkService = PokemonService()
        
        pkService.getPokemonImage(Id: (pokemon?.id)!){(pkImage) in
               self.pokemonImageView.image = pkImage
        }
        //pkService.downloadImage(Id: (pokemon?.id)!){(pkImage) in
         //   self.pokemonImageView.image = pkImage
        //}
    }
    
    
    
}
