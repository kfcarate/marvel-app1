//
//  PokemonViewController.swift
//  Marvel app
//
//  Created by kevin on 29/11/17.
//  Copyright © 2017 kevin. All rights reserved.
//

import UIKit

class PokemonViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate{
    
    @IBOutlet weak var pokedeskTableView: UITableView!
    var pokemonArray:[Pokemon] = []
    var pokemonIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let service = PokemonService()
        service.delegate = self
        service.dowloaderPokemon()
        // Do any additional setup after loading the view.
    }

    func get20FirstPokemons(pokemons: [Pokemon]) {
        //print(pokemons)
        pokemonArray = pokemons
        pokedeskTableView.reloadData()
    }
    
    func  numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch section {
//        case 0:
//            return 5
//        default:
//            return 10
//        }
        return pokemonArray.count
    }
    
    
    
    //metodo que devuelve data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
        //let cell = UITableViewCell()
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        cell.FillData(pokemon: pokemonArray[indexPath.row])
        //cell.textLabel?.text = "\(indexPath)"
        //cell.textLabel?.text = pokemonArray[indexPath.row].name
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "section 1"
        default:
            return "section 2"
        }
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        pokemonIndex = indexPath.row
        return indexPath
    }
    
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pokemonIndex = indexPath.row
    }*/
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let pokemonDetail = segue.destination as! DetailViewController
        pokemonDetail.pokemon = pokemonArray[pokemonIndex]
    }

}
