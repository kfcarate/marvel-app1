//
//  PokemonService.swift
//  Marvel app
//
//  Created by kevin on 2/1/18.
//  Copyright © 2018 kevin. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire
import AlamofireImage

protocol PokemonServiceDelegate{
    func get20FirstPokemons(pokemons:[Pokemon])
}

class PokemonService {
    
    var delegate:PokemonServiceDelegate?
    func dowloaderPokemon(){
        
        var pokemonArray:[Pokemon] = []
        let  dpGR = DispatchGroup()
        for i in 1...5{
            dpGR.enter()
                Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseObject
                    { (response: DataResponse<Pokemon>) in
                        
                        let pokemon = response.result.value
                        pokemonArray.append(pokemon!)
                        dpGR.leave()
                }
                /*if i == 20 {
                    delegate?.get20FirstPokemons(pokemons: pokemonArray)
                }*/
            }
        dpGR.notify(queue: .main) {
            let sortedArray = pokemonArray.sorted(by: {$0.id! < $1.id!})
            self.delegate?.get20FirstPokemons(pokemons: sortedArray)
        }
    }

    
        func getPokemonImage(Id:Int, completion: @escaping (UIImage) -> ()){
            
            Alamofire.request("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(Id).png").responseImage
                { response in
                    debugPrint(response)
                    if let image = response.result.value {
                        //print("image downloaded: \(image)")
                        completion(image)
                    }
            }
        }
    
}



