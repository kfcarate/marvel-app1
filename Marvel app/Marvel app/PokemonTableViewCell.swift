//
//  PokemonTableViewCell.swift
//  Marvel app
//
//  Created by kevin on 3/1/18.
//  Copyright © 2018 kevin. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {


    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var HeighLabel: UILabel!
    @IBOutlet weak var WeightLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func FillData(pokemon:Pokemon){
        
        NameLabel.text = pokemon.name
        HeighLabel.text = "\(pokemon.height ?? 0)"
        //HeighLabel.text = pokemon.experience
        WeightLabel.text = "\(pokemon.weight ?? 0)"
        
    }
    
}
